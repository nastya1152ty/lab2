let templateState = '<label name="%id%">' +
    '<input type="checkbox"/>' +
    '<p>%title%</p>' +
    '</label>'

let items = ''
let stateBase = {}

fetch('http://localhost:3000/state').then((answer) => {
    return answer.json()
}).then(data => {
    stateBase = data
    for (let item in stateBase) {
        let show = structuredClone(templateState)
        let obj = stateBase[item]
        show = show
            .replace('%id%', obj.showName)
            .replace('%title%', obj.showName)
        items += show
    }
    document.getElementById('container-state').innerHTML = items
})

document.getElementById('calculate').addEventListener('click', async () => {
    let localState = structuredClone(stateBase)
    for (let item of document.getElementById('container-state')?.children) {
        localState[item.attributes.name.textContent].node.value = item.children[0].checked
    }
    let answer = await fetch('http://localhost:3000/getExecutionAction', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify(localState)
    })
    let data = await answer.json()
    let historyStr = ''
    for (let history of data) {
        historyStr += '<p>Q('
        for(let item in history.state){
            historyStr += `${history.state[item].node.name}: `
            historyStr += (history.state[item].node.value ? stateBase[item].node.discriptionTrue : stateBase[item].node.discriptionFalse) + ', '
        }
        historyStr += ')</p>'
        for(let item of history.action){
            historyStr += '<p>'
            if (item.type === 'Obj') {
                historyStr += `${item.name}: `
                historyStr += item.value ? stateBase[item.name].node.discriptionTrue : stateBase[item.name].node.discriptionFalse
            }
            if (item.type === 'Rel') {
                historyStr += item.name1 + item.name2
            }
            historyStr += ','
            historyStr += '</p>'
        }
    }
    document.getElementById('history').innerHTML = historyStr
})