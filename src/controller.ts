import base from './base.json' assert { type: "json" };
import fs from 'fs'

import { ObjectState, ReletionState, nodeState, nodeBase } from './type.js'

export class App {
    finalState?: nodeBase
    stateBase: { [key: string]: nodeState } = {}
    qustionsState: any = []
    baseAction: {
        [key: string]: {
            state: nodeBase,
            action: any
        }
    } = {}

    constructor() {
        this.initState(base)
        let count = 0
        let data = fs.readFileSync('rules.txt', 'utf8')
        for (let item of data.split('\n')) {
            count++
            let parse = item.split('ЕСЛИ')[1].trim()
            let arr = parse.split('ТО')
            let state = arr[0]
            let action = arr[1]
            let parseState: { [key: string]: nodeState } = {}
            let arrState = []
            for (let el of state.split(' и ')) {
                let val1 = el.split('=')[0].trim()
                let val2 = el.split('=')[1].trim()
                let obj: any = {
                    name: val1,
                    discrTrue: '',
                    discrFalse: '',
                    value: false
                }
                obj.value = val2.includes('не') ? false : true
                arrState.push(obj)
            }
            arrState.sort((a, b) => {
                return a.name > b.name ? -1 : 1
            })
            for (let item of arrState) {

                parseState[item.name] = new nodeState({
                    name: item.name,
                    discrTrue: item.discrTrue,
                    discrFalse: item.discrFalse
                }, 'obj')
                parseState[item.name].node!.value = item.value
            }
            let base = new nodeBase(parseState)
            let parseAction = {
                name: action.split('=')[0].trim(),
                value: action.split('=')[1].includes('не') && !action.split('=')[1].includes('нет') ? false : true,
                type: 'Obj'
            }
            this.baseAction[base.getValue()] = {
                state: base,
                action: parseAction
            }
        }
    }

    initState = (file: any) => {
        for (let item of base.object) {
            this.stateBase[item.name] = new nodeState(item, 'obj')
        }
        let localFinal: { [key: string]: nodeState } = {}
        let arr: any = []
        for (let item of base.finalStae) {
            let obj = new nodeState(item, 'obj')
            obj.node!.value = item.value
            arr.push({ value: obj, name: item.name })
        }
        arr.sort((a: any, b: any) => {
            return a.name > b.name ? -1 : 1
        })
        for (let item of arr) {
            localFinal[item.name] = item.value
        }
        this.finalState = new nodeBase(localFinal)
    }

    initAction = (file: any) => {
        for (let item of file) {
            let localState = new nodeBase(this.stateBase)
            for (let el of item.state) {
                if (el.type === 'Obj') {
                    localState.nodeState[el.name].node!.value = el.value
                }
                if (el.type === 'Rel') {
                    localState.nodeState[el.name1 + ';' + el.name2].node!.value = el.value
                }
            }
            let action = item.action
            if (item.qustionAction) {
                this.qustionsState.push({
                    qustion: item.qustionAction,
                    state: localState
                })
            }
            this.baseAction[localState.getValue()] = {
                state: localState,
                action: action
            }
        }
        this.finalState = new nodeBase(this.stateBase)
        for (let el of base.finalStae) {
            if (el.type === 'Obj' && typeof el.name !== 'undefined') {
                this.finalState.nodeState[el.name].node!.value = el.value
            }
            // if (el.type === 'Rel') {
            //     this.finalState.nodeState[el.name1 + ';' + el.name2].node!.value = el.value
            // }
        }
    }

    getExecutionAction = (currentState: any) => {
        let history: any = []
        let that = this
        let arr: any = []
        for (let item in currentState) {
            let obj = new nodeState(currentState[item].node, 'obj')
            obj.node!.value = currentState[item].node.value
            arr.push({ value: obj, name: item })
        }
        arr.sort((a: any, b: any) => {
            return a.name > b.name ? -1 : 1
        })
        let localState: any = {}
        for (let item of arr) {
            localState[item.name] = item.value
        }
        let state: nodeBase = new nodeBase(localState)
        function executionAction() {
            let pos = state.getValue()
            if (that.finalState!.getValue() === pos) return
            if (that.baseAction[pos]) {
                let historyObj: { state: any, action: any } = {
                    state: JSON.parse(JSON.stringify(state.nodeState)),
                    action: []
                }
                if (Array.isArray(that.baseAction[pos].action)) {
                    for (let item of that.baseAction[pos].action) {
                        state = that.changeState(item, state)
                        historyObj.action.push(item)
                    }
                }
                else {
                    state = that.changeState(that.baseAction[pos].action, state)
                    historyObj.action.push(that.baseAction[pos].action)
                }
                history.push(historyObj)
                executionAction()
            }
            return
        }

        executionAction()
        history.push({
            state: state.nodeState,
            action: []
        })
        return history
    }

    changeState = (item: any, el: nodeBase): nodeBase => {
        if (item.type === 'Obj') {
            el.nodeState[item.name].node!.value = item.value
        }
        if (item.type === 'Rel') {
            el.nodeState[item.name1 + ';' + item.name2].node!.value = item.value
        }
        return el
    }
}