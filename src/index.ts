import base from '../base.json'
import action from '../baseAction.json'
import { ObjectState, ReletionState, nodeState, nodeBase } from './type.js'

let stateBase: { [key: string]: nodeState } = {}

let baseAction: {
    [key: string]: {
        state: nodeBase,
        action: any
    }
} = {}

let currentState: nodeBase
let finalState: nodeBase
let history: nodeBase[] = []

let qustionsState: any = []

const initState = (file: any) => {
    for (let item of base.object) {
        stateBase[item.name] = new nodeState(item, 'obj')
    }
    for (let item of base.relation) {
        let objectLeft = stateBase[item.nameObj1].node
        let objectRight = stateBase[item.nameObj2].node
        let rel = new nodeState(item, 'rel', objectLeft as ObjectState, objectRight as ObjectState)
        stateBase[rel.getName()] = rel
    }
}

const initAction = (file: any) => {
    let count = 0
    for (let item of file) {
        count++
        let localState = new nodeBase(stateBase)
        for (let el of item.state) {
            if (el.type === 'Obj') {
                localState.nodeState[el.name].node.value = el.value
            }
            if (el.type === 'Rel') {
                localState.nodeState[el.name1 + ';' + el.name2].node.value = el.value
            }
        }
        let action = item.action
        if (item.qustionAction) {
            qustionsState.push({
                qustion: item.qustionAction,
                state: localState
            })
        }
        baseAction[localState.getValue()] = {
            state: localState,
            action: action
        }
        if (count === file.length) {
            // console.log(finalState, action)
        }
    }
    finalState = new nodeBase(stateBase)
    for (let el of base.finalStae) {
        if (el.type === 'Obj') {
            finalState.nodeState[el.name].node.value = el.value
        }
        if (el.type === 'Rel') {
            finalState.nodeState[el.name1 + ';' + el.name2].node.value = el.value
        }
    }
}

const executionAction = () => {
    let pos = currentState.getValue()
    if (finalState.getValue() === pos) return
    if (baseAction[pos]) {
        if (Array.isArray(baseAction[pos].action)) {
            for (let item of baseAction[pos].action) {
                currentState = changeState(item, currentState)
            }
        }
        else {
            currentState = changeState(baseAction[pos].action, currentState)
        }
        history.push(new nodeBase(currentState.nodeState))
        executionAction()
    }
    else {
        let count = 0
        for (let item of qustionsState) {
            count++
            let check = confirm(item.qustion)
            if (!check) {
                currentState = new nodeBase(item.state.nodeBase)
                break
            }
            if (check && count === qustionsState.length) {
                currentState = new nodeBase(finalState.nodeState)
                history.push(new nodeBase(currentState.nodeState))
            }
        }
        if (count === qustionsState.length) {
            return
        }
        executionAction()
    }
    return
}

const changeState = (item: any, el: nodeBase): nodeBase => {
    if (item.type === 'Obj') {
        el.nodeState[item.name].node.value = item.value
    }
    if (item.type === 'Rel') {
        el.nodeState[item.name1 + ';' + item.name2].node.value = item.value
    }
    return el
}

initState(base)
initAction(action)
