import url from 'url';
import express from 'express';
import path from 'path';
import { dirname } from 'path';
import {App} from './controller.js'

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = dirname(__filename) + '/..';
const appCalc = new App()

const port = 3000
const app = express()

app.use(express.static(__dirname + "/public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get("/", function (request, response) {
    response.sendFile(path.resolve(__dirname + '/index.html'))
});

app.get('/state', function (request, response) {
    let state = appCalc.stateBase
    let answer: { [key: string]: object } = {}
    for (let item in state) {
        answer[item] = {}
        answer[item] = {...state[item], showName: state[item].getName()}
    }
    response.send(JSON.stringify(answer))
})

app.post('/getExecutionAction', function (request, response) {

    let data = request.body
    let dataParce = JSON.parse(Object.keys(data)[0])
    let answer = appCalc.getExecutionAction(dataParce)
    response.send(JSON.stringify(answer))

})

app.listen(port, () => {
    console.log(`server http://localhost:${port}`)
})
