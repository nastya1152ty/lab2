export interface ObjectState {
    name: string,
    value: Boolean,
    discriptionTrue: string,
    discriptionFalse: string
}

export interface ReletionState {
    objectLeft: ObjectState,
    objectRight: ObjectState,
    value: Boolean
}

interface InodeState {
    node?: ObjectState | ReletionState | object,
    type: string
    getName(): string
}


export class nodeState implements InodeState {
    node?: ObjectState | ReletionState
    type: string

    constructor(obj: any, type: string, objectLeft?: ObjectState, objectRight?: ObjectState) {
        this.type = type
        if (type === 'obj') {
            this.node = {
                name: obj.name,
                discriptionTrue: obj.discrTrue,
                discriptionFalse: obj.discrFalse,
                value: false,
            } as ObjectState
        }
        if (type === 'rel') {
            this.node = {
                objectLeft: objectLeft,
                objectRight: objectRight,
            } as ReletionState
        }
    }
    getName(): string {
        if (this.type === 'obj') {
            return (this.node as ObjectState).name
        }
        if (this.type === 'rel') {
            let node = (this.node as ReletionState)
            return node.objectLeft.name + ';' + node.objectRight.name
        }
        return ''
    }
}

interface InodeBase {
    nodeState: { [key: string]: nodeState },
    getValue(): string
}

export class nodeBase implements InodeBase {
    nodeState: { [key: string]: nodeState } = {}
    constructor(node: { [key: string]: nodeState }) {
        for (let item in node) {
            this.nodeState[item] = JSON.parse(JSON.stringify(node[item])) as nodeState
            this.nodeState[item].getName = node[item].getName
        }
    }
    getValue(): string {
        let answer = ''
        for (let item in this.nodeState) {
            let val = this.nodeState[item].node
            if (typeof val !== 'undefined') {
                answer += val.value ? val.value.toString() : 'false'
            }
        }
        return answer
    }
}
